provincias = {}


def añadir_distr_provin(distr, provin):
    global provincias
    if not provin in provincias:
        provincias[provin] = []
    provincias[provin].append(distr)


def imprimir_distr_de_provin(provin):
    if not provin in provincias:
        print("No hay distritos registrados")
        return

    print("Los distritos de " + provin + " son: ")
    for distr in provincias[provin]:
        print(distr)


eleccion = ""
while eleccion != "3":
    eleccion = input("""
1. Añadir distrito
2. Revelar distritos
3. Salir
Seleccione: """)
    if eleccion == "1":
        distr = input("Ingrese el distrito: ")
        provin = input("Ingrese la provincia al que pertenece: ")
        añadir_distr_provin(distr, provin)
        print("Agregado")
        pass
    elif eleccion == "2":
        provin = input("Ingrese la provincia: ")
        imprimir_distr_de_provin(provin)
        pass
